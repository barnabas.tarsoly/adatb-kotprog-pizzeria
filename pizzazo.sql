-- phpMyAdmin SQL Dump
-- version 5.3.0-dev+20221125.2e001c186a
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2022 at 09:33 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizzazo`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `pizzaId` int(11) NOT NULL,
  `delivered` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `userId`, `pizzaId`, `delivered`) VALUES
(74, 51, 217, 0),
(75, 51, 217, 0),
(76, 51, 218, 0),
(77, 52, 219, 0),
(78, 53, 217, 0),
(79, 52, 220, 0),
(80, 51, 224, 0),
(81, 54, 218, 0),
(82, 55, 216, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pizza`
--

CREATE TABLE `pizza` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_hungarian_ci NOT NULL,
  `type` text COLLATE utf8mb4_hungarian_ci NOT NULL,
  `price` int(11) NOT NULL,
  `img` text COLLATE utf8mb4_hungarian_ci DEFAULT NULL,
  `isDeleted` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- Dumping data for table `pizza`
--

INSERT INTO `pizza` (`id`, `name`, `type`, `price`, `img`, `isDeleted`) VALUES
(216, 'Após kedvence pizza', 'Paradicsomos alap', 5890, 'https://turbopizza.hu/image/cache/catalog/pizza/bbq_csirkes-250x250.jpg', 0),
(217, 'Baconos pizza', 'Paradicsomos alap', 5090, 'https://turbopizza.hu/image/cache/catalog/pizza/Bacon-250x250.jpg', 0),
(218, 'Bbq csirkés pizza', 'Házi BBQ alap', 5890, 'https://turbopizza.hu/image/cache/catalog/pizza/Csirkes3-250x250.jpg', 0),
(219, 'Csirkés pizza', 'Tejfölös alap', 5390, 'https://turbopizza.hu/image/cache/catalog/pizza/Aposkedvence3-250x250.jpg', 0),
(220, 'Gombás pizza', 'Paradicsomos alap', 5090, 'https://turbopizza.hu/image/cache/catalog/pizza/Csirkes3-250x250.jpg', 0),
(221, 'Gyros pizza', 'Tzatziki alap', 5690, 'https://turbopizza.hu/image/cache/catalog/pizza/Honeymoon6-250x250.jpg', 0),
(222, 'Hawaii pizza', 'Paradicsomos alap', 5390, 'https://turbopizza.hu/image/cache/catalog/pizza/Honeymoon6-250x250.jpg', 0),
(223, 'Honeymoon pizza', 'Paradicsomos alap', 5890, 'https://turbopizza.hu/image/cache/catalog/pizza/Csirkes3-250x250.jpg', 0),
(224, 'Húsimádó pizza', 'Paradicsomos alap', 5890, 'https://turbopizza.hu/image/cache/catalog/pizza/Husimado3-250x250.jpg', 0),
(225, 'Ínyenc pizza', 'Tejfölös alap', 5690, 'https://turbopizza.hu/image/cache/catalog/pizza/bbq_csirkes-250x250.jpg', 0),
(226, 'Ízbomba pizza', 'Fokhagymás-paradicsomos alap', 5890, 'https://turbopizza.hu/image/cache/catalog/pizza/Husimado3-250x250.jpg', 0),
(227, 'Karcsi kedvence pizza', 'Paradicsomos alap', 5390, 'https://turbopizza.hu/image/cache/catalog/pizza/Csirkes3-250x250.jpg', 0),
(228, 'Kolbászos pizza', 'Paradicsomos alap', 5090, 'https://turbopizza.hu/image/cache/catalog/pizza/bbq_csirkes-250x250.jpg', 0),
(229, 'Magyaros pizza', 'Paradicsomos alap', 5690, 'https://turbopizza.hu/image/cache/catalog/pizza/Csirkes3-250x250.jpg', 0),
(230, 'Margaréta pizza', 'Paradicsomos alap', 5090, 'https://turbopizza.hu/image/cache/catalog/pizza/Aposkedvence3-250x250.jpg', 0),
(231, 'Mézes mustáros csirke pizza', 'Mézes mustáros alap', 5890, 'https://turbopizza.hu/image/cache/catalog/pizza/Honeymoon6-250x250.jpg', 0),
(232, 'Óhaj-sóhaj pizza', 'Tetszőleges alap', 5990, 'https://turbopizza.hu/image/cache/catalog/pizza/bbq_csirkes-250x250.jpg', 1),
(233, 'Olivás-fetás pizza', 'Paradicsomos alap', 5390, 'https://turbopizza.hu/image/cache/catalog/pizza/Husimado3-250x250.jpg', 0),
(234, 'Ördögi pizza', 'Paradicsomos alap', 5690, 'https://turbopizza.hu/image/cache/catalog/pizza/Husimado3-250x250.jpg', 0),
(235, 'Piedone pizza', 'Paradicsomos alap', 5690, 'https://turbopizza.hu/image/cache/catalog/pizza/gyros2-250x250.jpg', 0),
(236, 'Sajtimádó pizza', 'Paradicsomos alap', 5690, 'https://turbopizza.hu/image/cache/catalog/pizza/Honeymoon6-250x250.jpg', 0),
(237, 'Sajtos pizza', 'Paradicsomos alap', 4790, 'https://turbopizza.hu/image/cache/catalog/pizza/gyros2-250x250.jpg', 0),
(238, 'Son-go-ku pizza', 'Paradicsomos alap', 5690, 'https://turbopizza.hu/image/cache/catalog/pizza/Csirkes3-250x250.jpg', 0),
(239, 'Sonkás pizza', 'Paradicsomos alap', 5090, 'https://turbopizza.hu/image/cache/catalog/pizza/Honeymoon6-250x250.jpg', 0),
(240, 'Sonkás-gombás pizza', 'Paradicsomos alap', 5390, 'https://turbopizza.hu/image/cache/catalog/pizza/Honeymoon6-250x250.jpg', 0),
(241, 'Sonkás-kukoricás pizza', 'Paradicsomos alap', 5390, 'https://turbopizza.hu/image/cache/catalog/pizza/Gombas3-250x250.jpg', 0),
(242, 'Szalámis pizza', 'Paradicsomos alap', 5090, 'https://turbopizza.hu/image/cache/catalog/pizza/Gombas3-250x250.jpg', 0),
(243, 'Tanyasi pizza', 'Házi BBQ alap', 5890, 'https://turbopizza.hu/image/cache/catalog/pizza/Honeymoon6-250x250.jpg', 0),
(244, 'Tarjás pizza', 'Paradicsomos alap', 5090, 'https://turbopizza.hu/image/cache/catalog/pizza/Csirkes3-250x250.jpg', 0),
(245, 'Vegetáriánus pizza', 'Paradicsomos alap', 5890, 'https://turbopizza.hu/image/cache/catalog/pizza/Bacon-250x250.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ptrelations`
--

CREATE TABLE `ptrelations` (
  `id` int(11) NOT NULL,
  `pizzaId` int(11) DEFAULT NULL,
  `toppingId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- Dumping data for table `ptrelations`
--

INSERT INTO `ptrelations` (`id`, `pizzaId`, `toppingId`) VALUES
(65, 217, 4),
(66, 217, 25),
(67, 218, 6),
(68, 218, 25),
(69, 218, 32),
(70, 218, 37),
(71, 219, 6),
(72, 219, 18),
(73, 219, 25),
(74, 220, 14),
(75, 220, 25),
(76, 221, 25),
(77, 221, 33),
(78, 221, 34),
(79, 222, 3),
(80, 222, 25),
(81, 222, 26),
(82, 223, 9),
(83, 223, 20),
(84, 223, 25),
(85, 223, 27),
(86, 223, 35),
(87, 224, 4),
(88, 224, 17),
(89, 224, 25),
(90, 224, 26),
(91, 224, 27),
(92, 225, 4),
(93, 225, 7),
(94, 225, 14),
(95, 225, 17),
(96, 225, 25),
(97, 226, 4),
(98, 226, 16),
(99, 226, 17),
(100, 226, 25),
(101, 227, 3),
(102, 227, 19),
(103, 227, 25),
(104, 227, 26),
(105, 228, 17),
(106, 228, 25),
(107, 229, 4),
(108, 229, 10),
(109, 229, 15),
(110, 229, 17),
(111, 229, 25),
(112, 230, 25),
(113, 230, 36),
(114, 231, 6),
(115, 231, 22),
(116, 231, 25),
(117, 231, 37),
(118, 233, 12),
(119, 233, 38),
(120, 234, 7),
(121, 234, 9),
(122, 234, 10),
(123, 234, 17),
(124, 234, 25),
(125, 235, 5),
(126, 235, 15),
(127, 235, 17),
(128, 235, 19),
(129, 235, 25),
(130, 236, 11),
(131, 236, 13),
(132, 236, 22),
(133, 236, 23),
(134, 237, 25),
(135, 237, 39),
(136, 238, 14),
(137, 238, 19),
(138, 238, 25),
(139, 238, 26),
(140, 239, 25),
(141, 239, 26),
(142, 240, 14),
(143, 240, 25),
(144, 240, 26),
(145, 241, 19),
(146, 241, 25),
(147, 241, 26),
(148, 242, 25),
(149, 242, 40),
(150, 243, 7),
(151, 243, 10),
(152, 243, 13),
(153, 243, 25),
(154, 243, 28),
(155, 243, 34),
(156, 244, 25),
(157, 244, 28),
(158, 245, 24),
(159, 245, 25),
(160, 245, 34),
(161, 245, 41),
(162, 245, 42),
(172, 216, 5),
(173, 216, 10),
(174, 216, 15),
(175, 216, 25),
(176, 216, 26),
(177, 216, 27),
(178, 216, 31);

-- --------------------------------------------------------

--
-- Table structure for table `topping`
--

CREATE TABLE `topping` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin2 COLLATE latin2_hungarian_ci NOT NULL,
  `price` int(100) NOT NULL,
  `isDeleted` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- Dumping data for table `topping`
--

INSERT INTO `topping` (`id`, `name`, `price`, `isDeleted`) VALUES
(3, 'Ananász', 600, 0),
(4, 'Bacon', 600, 0),
(5, 'Bab', 500, 0),
(6, 'Csirke sonka', 600, 0),
(7, 'Csemege uborka', 500, 0),
(8, 'Csirkemell', 900, 0),
(9, 'Chili paprika', 500, 0),
(10, 'Erős paprika', 500, 0),
(11, 'Fetasajt', 600, 0),
(12, 'Fekete oliva', 500, 0),
(13, 'Füstölt sajt', 800, 0),
(14, 'Gomba', 500, 0),
(15, 'Hagyma', 500, 0),
(16, 'Jalapeno paprika', 600, 0),
(17, 'Kolbász', 600, 0),
(18, 'Kaliforniai paprika', 500, 0),
(19, 'Kukorica', 500, 0),
(20, 'Méz', 500, 0),
(21, 'Lilahagyma', 500, 0),
(22, 'Mozzarella', 800, 0),
(23, 'Parmezan', 600, 0),
(24, 'Paradicsom', 500, 0),
(25, 'Sajt', 800, 0),
(26, 'Sonka', 600, 0),
(27, 'Szalámi', 600, 0),
(28, 'Tarja', 600, 0),
(29, 'Tojás', 600, 0),
(30, 'Zöld oliva', 500, 0),
(31, 'Chili', 300, 0),
(32, 'Tükörtojás', 600, 0),
(33, 'Gyros hús', 500, 0),
(34, 'Lila hagyma', 300, 0),
(35, 'Mozzarella sajt', 400, 0),
(36, 'Paradicsom karika', 300, 0),
(37, 'Csirke mell', 700, 0),
(38, 'Feta sajt', 300, 0),
(39, 'Oregánó', 600, 0),
(40, 'Pick szalámi', 400, 0),
(41, 'Feta', 500, 0),
(42, 'Oliva', 600, 0),
(43, 'Mindent bele', 3000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin2 COLLATE latin2_hungarian_ci NOT NULL,
  `phone` varchar(12) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `email` varchar(200) COLLATE utf8mb4_hungarian_ci NOT NULL,
  `address` varchar(500) CHARACTER SET latin2 COLLATE latin2_hungarian_ci NOT NULL,
  `isDeleted` tinyint(1) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_hungarian_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `phone`, `email`, `address`, `isDeleted`) VALUES
(51, 'Barni', '+36202658168', 'barnus2013@gmail.com', '4948, asdasdas asd', 0),
(52, 'Jakab Béla', '+36201848886', 'jakab.bela@gmail.com', '6969, LekVaros Zsák utca 1', 0),
(53, 'Jakab Béla', '+36202148488', 'jakab.bela@gmail.com', '6969, LekVaros Zsák utca 1', 0),
(54, 'sdf fsdfe', '+36202658168', 'asd@asd.com', '6969, LekVaros Zsák utca 1', 0),
(55, 'János', '+36202658168', 'asdasd@janos.com', '4948, asdasdas asd', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pizzaRelation` (`pizzaId`),
  ADD KEY `userRelation` (`userId`);

--
-- Indexes for table `pizza`
--
ALTER TABLE `pizza`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ptrelations`
--
ALTER TABLE `ptrelations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ptrelations_ibfk_1` (`pizzaId`),
  ADD KEY `ptrelations_ibfk_2` (`toppingId`);

--
-- Indexes for table `topping`
--
ALTER TABLE `topping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `pizza`
--
ALTER TABLE `pizza`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=252;

--
-- AUTO_INCREMENT for table `ptrelations`
--
ALTER TABLE `ptrelations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `topping`
--
ALTER TABLE `topping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `pizzaRelation` FOREIGN KEY (`pizzaId`) REFERENCES `pizza` (`id`),
  ADD CONSTRAINT `userRelation` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

--
-- Constraints for table `ptrelations`
--
ALTER TABLE `ptrelations`
  ADD CONSTRAINT `ptrelations_ibfk_1` FOREIGN KEY (`pizzaId`) REFERENCES `pizza` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `ptrelations_ibfk_2` FOREIGN KEY (`toppingId`) REFERENCES `topping` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
