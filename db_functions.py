

from flask_mysqldb import MySQL
import json

class DbConnections:
    def __init__(self, app) -> None:
        self.app = app
        self.mysql = MySQL(app)
        # cursor = None

    # def setCursor(self):
        
    def get_all_pizza(self):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f'SELECT * FROM `pizza` WHERE NOT `isDeleted`')
        data = cursor.fetchall()
        result = []

        for x in data:
            toppings = [topping[0] for topping in self.get_toppings_for_pizza(x[0])]
            result.append({'id':x[0], 'name':x[1],'type':x[2],'price':x[3], 'img': x[4], 'toppings':toppings})

        return result    

    def list_pizza(self):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f'SELECT * FROM `pizza` WHERE NOT `isDeleted`')
        data = cursor.fetchall()
        result = []

        for x in data:
            toppings = [topping[0] for topping in self.get_toppings_for_pizza(x[0])]
            result.append({'id':x[0], 'name':x[1],'type':x[2],'price':x[3], 'img': x[4], 'toppings':', '.join(toppings)})

        return result  
    def save_pizza(self, pizza):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f"INSERT INTO `pizza` (`id`, `name`, `type`, `price`, `img`) VALUES (NULL, '{pizza['name']}', '{pizza['type']}', '{pizza['price']}', {pizza['img']});")
        self.mysql.connection.commit()
        self.set_pt_relations(cursor.lastrowid, pizza['toppings'])
        # print(cursor.lastrowid)

    def delete_pizza(self, pizza_id):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f"UPDATE pizza SET `isDeleted` = 1 WHERE `pizza`.`id` = '{pizza_id}';") #Never delete actually
        self.mysql.connection.commit()

    def edit_pizza(self, pizza_id, pizza):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f"UPDATE `pizza` SET name = '{pizza['name']}', type = '{pizza['type']}', price = '{pizza['price']}', img = '{pizza['img']}' WHERE pizza.id = {pizza_id};")
        self.mysql.connection.commit()
        self.edit_pt_relations(pizza_id, pizza['toppings'])

    def get_toppings(self):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f'SELECT * FROM `topping` WHERE NOT `isDeleted`')
        data = cursor.fetchall()
        result = []
        for x in data:
            result.append({"id":x[0],'name':x[1],'price':x[2]})
        # print(result)
        return result
    
    def save_topping(self,topping):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f"INSERT INTO `topping` (`name`, `price`) VALUES ('{topping['name']}', '{topping['price']}');")
        self.mysql.connection.commit()

    def delete_topping(self,topping_name):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f"DELETE topping WHERE `topping`.`name` = '{topping_name}';")
        self.mysql.connection.commit()

    def edit_pt_relations(self, pizzaId, toppings):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f"DELETE FROM `ptrelations` WHERE `ptrelations`.`pizzaId` = {pizzaId}")
        self.mysql.connection.commit()
        self.set_pt_relations(pizzaId, toppings)

    def set_pt_relations(self,pizza, toppings):
        cursor = self.mysql.connection.cursor()
        for topping in toppings:
              print(topping)
              cursor.execute(f"INSERT INTO `ptrelations` (`pizzaId`, `toppingId`) VALUES ('{pizza}','{topping}')")
        self.mysql.connection.commit()  

    def get_toppings_for_pizza(self, pizzaId):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f"SELECT name FROM `topping` WHERE id IN (SELECT toppingId from ptrelations where pizzaId = {pizzaId})")
        data = cursor.fetchall()
        return data

    def get_pizza_by_id(self, pizzaId):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f"SELECT * FROM `pizza` WHERE id = {pizzaId}")
        data = cursor.fetchall()
        toppings = [topping[0] for topping in self.get_toppings_for_pizza(pizzaId)]
        return {'id':data[0][0], 'name':data[0][1],'type':data[0][2],'price':data[0][3], 'img': data[0][4], 'toppings':toppings}

    def save_order(self, order):
        cursor = self.mysql.connection.cursor()
        #search for user id and if not exist create new user
        cursor.execute(f"Select userId, count(*) from orders where userId = (SELECT id AS userID FROM `user` WHERE name = '{order['name']}' AND email = '{order['email']}' AND address = '{order['address']}' AND phone = '{order['phone']}');")
        data = cursor.fetchall()
        print(data)
        if data[0][0] == None:
            cursor.execute(f"INSERT INTO `user` (`name`, `email`, `address`, `phone`) VALUES ('{order['name']}', '{order['email']}', '{order['address']}', '{order['phone']}');")
            self.mysql.connection.commit()
            userId = cursor.lastrowid
            orderCount = 1
        else:
            userId = data[0][0]
            
            # orderCount = 1
            # print(data)
            orderCount = data[0][1] + 1

        
        # save user
        
        # save order
        # userId = cursor.lastrowid
        cursor.execute(f"INSERT INTO `orders` (`id`, `userId`, `pizzaId`) VALUES (NULL, '{userId}', '{order['pizza']}');")
        
        self.mysql.connection.commit()

        return (cursor.lastrowid, orderCount)

    def get_orders(self):
        cursor = self.mysql.connection.cursor()

        cursor.execute(f"SELECT user.*, sum(price) from user, pizza, (SELECT userId as userID, pizzaId as pizzaID from orders WHERE delivered = 0) as deriverable where user.id = deriverable.userID and pizza.id = deriverable.pizzaID GROUP BY user.id;")
        userFilter = cursor.fetchall()


        users = []
        for user in userFilter:
            user_data = {'name':user[1], 'email':user[3], 'address':user[4], 'phone':user[2], 'total':user[6]}
            cursor.execute(f"SELECT user.*, pizza.*, orderID from user, pizza, (SELECT userId as userID, pizzaId as pizzaID, id as orderID from orders WHERE delivered = 0 AND userId = {user[0]}) as deriverable where user.id = deriverable.userID and pizza.id = deriverable.pizzaID;")
            data = cursor.fetchall()
            result = []
            for x in data:
                result.append({'pizza':x[7], 'type':x[8], 'price':x[9], 'orderId': x[12]})
            users.append({'user':user_data, 'orders':result})
        
        print(users)

        return users

    def deliver_order(self, orderId):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f"UPDATE `orders` SET `delivered` = '1' WHERE `orders`.`id` = {orderId};")
        self.mysql.connection.commit()
    
    def delete_order(self, orderId):
        cursor = self.mysql.connection.cursor()
        cursor.execute(f"DELETE FROM `orders` WHERE `orders`.`id` = {orderId};")
        self.mysql.connection.commit()
