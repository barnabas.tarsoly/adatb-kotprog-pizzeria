from flask import Flask,render_template, request, redirect, url_for, session
from flask_bootstrap import Bootstrap5
from db_functions import *
app = Flask(__name__)
app.secret_key = '123456789'


Bootstrap5(app)
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_DB'] = 'pizzazo'
db_conn = DbConnections(app) 


pizza_types = ['Paradicsomos alap', 'Csípős paradicsomos', 'Tejfölös', 'Fokhagymás-tejfölös', 'Tejfölös-paradicsomos','Csípős-tejfölös','Tejfölös-mustáros','BBQ']



@app.route('/')
def form():
    return render_template('index.html', pizzas=db_conn.list_pizza())
 
@app.route('/add-pizza', methods=['POST' ,'GET'])
def add_pizza():
    form = request.form

    if request.method == 'POST':
        toppings = []
        for key in list(form)[3:]:

            toppings.append(form[key])
        pizza = {'name':form['name'],'type':form['type'],'price':form['price'],'toppings':toppings, 'img':form['img']}
        db_conn.save_pizza(pizza)
    return render_template('add_pizza.html', toppings=db_conn.get_toppings(), pizzas=db_conn.get_all_pizza(), pizza_types=pizza_types)

@app.route('/add-topping', methods=['POST'])
def add_topping():
    form = request.form
    topping = {'name':form['name'],'price': form['price']}
    db_conn.save_topping(topping)
    return redirect(url_for('add_pizza'))

@app.route('/edit-pizza', methods=['POST'])
def edit_pizza_route():
    try:
        pizza_id_delete = request.form['delete']
        db_conn.delete_pizza(pizza_id_delete)
    except: pass
    try:
        pizza_id_edit = request.form['save']
        form = request.form
        toppings = []
        for key in list(form)[3:-2]:

            toppings.append(form[key])
        pizza = {'name':form['name'],'type':form['type'],'price':form['price'],'toppings':toppings, 'img': form['img']}
        

        db_conn.edit_pizza(pizza_id_edit, pizza)
    
    except Exception as e:
        print(e)
    return redirect(url_for('add_pizza'))

@app.route('/remove-topping', methods=['POST'])
def remove_topping():
    try:
        topping_name = request.form['delete']
        db_conn.delete_topping(topping_name)
    except Exception as e:
        print(e)
    return redirect(url_for('add_pizza'))

@app.route('/cart/<int:id>', methods=['POST', 'GET', 'DELETE'])
def cart_items(id):
    if request.method == 'POST':
        print(id)
        if 'cart' in session:
           session['cart'] = id
        else:
           session['cart'] = id
    return redirect(url_for('cart'))

@app.route('/cart-clear', methods=['POST'])
def cart_clear():
    session.clear()
    return redirect(url_for('cart'))

@app.route('/cart', methods=['POST', 'GET'])
def cart():
    if 'cart' in session:
        pizza = db_conn.get_pizza_by_id(session['cart'])
        print(pizza)
        return render_template('cart.html', pizza=pizza)
    else:
        return render_template('cart.html',pizza=None)

@app.route('/order', methods=['POST', 'GET'])
def order():
    try:

        orderId = request.args['order']
    except:
        orderId = None 
        severity = 'warning'
    if orderId:
        message = 'Sikeres rendelés! Rendelés azonosító: #' + orderId + '.' + ' Ez a ' + request.args['orderCount'] + '. rendelésed.'
        severity = 'success'
    else:
        message = 'Nincs folyamatban lévő rendelés!'
    if request.method == 'POST':
        form = request.form
        order = {'name':form['name'],'phone':form['phone'],'address':form['address'],'email':form['email'], 'pizza':form['pizza']}
        res = db_conn.save_order(order)
        session.clear()
        return redirect(url_for('order',order=res[0], orderCount=res[1]))
    return render_template('order.html',message=message, severity=severity)

@app.route('/orders', methods=['POST', 'GET'])
def orders():
    return render_template('orders.html', user_orders=db_conn.get_orders())

@app.route('/resolve-order/<int:id>', methods=['POST', 'GET'])
def resolve_order(id):
    db_conn.deliver_order(id)
    return redirect(url_for('orders'))

@app.route('/delete-order/<int:id>', methods=['POST', 'GET'])
def delete_order(id):
    db_conn.delete_order(id)
    return redirect(url_for('orders'))


app.run(host='localhost', port=5000, debug=True)



